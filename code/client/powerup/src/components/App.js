import React from "react";
import ArticlesList from './ArticlesList';
import powerup from "../apis/powerup";

class App extends React.Component{
    state = {articles: []}
    componentDidMount(){
       this.getArticle();
    }

   async getArticle() {
        const response = await powerup.get('articles/');
    this.setState({articles:response.data.data.articles });
    }


    render(){
        return (
            <div><ArticlesList articlesList ={this.state.articles} /></div>
        );
    }
}

export default App;
