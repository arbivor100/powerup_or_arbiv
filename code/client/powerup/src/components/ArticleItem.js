import React from 'react';

const ArticleItem = ({article}) =>{
return(
    <div className="ui segment">
    <div className="content">
        <div className="header">
            <div className="content"><img className="ui avatar Mini floated image" src={article.source_image} /></div>
            <i className="right floated heart icon" onClick={(e) => e.target.style.color = 'blue'} style={{position: "absolute", top:"10px", right:"10px"}}></i>
            <div className="content" style={{color:'blue'}}>{article.publisher}</div>
            <div className="content" style={{color:'gray'}}>{article.created}</div>
            <div className="content"><img className="ui right floated image" style={{width:"300px"}} src={article.image} /></div>
            <br/>
        </div>
        <div className="content">
            <div className="ui header">{article.title}</div>
            <div className="content">{article.description}</div>
            <br/>
        </div>
        <div className="content">
            <a href={article.url} target="_blank" rel="noopener noreferrer">read more... </a>
        </div>
    </div>
    </div>

);
};
export default ArticleItem;
