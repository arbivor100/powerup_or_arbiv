import React from 'react'
import ArticleItem from "./ArticleItem";

const ArticlesList= ({articlesList}) => {
    console.log({articlesList});
    const listOfArticles = articlesList.map((article) => {
        return <ArticleItem key={article.id} article={article}/>
    });


    return (
        <div className="ui relaxed divided list">{listOfArticles}</div>
    );
}

export default ArticlesList;